---
layout: post
title: "Supporter spotlight: Civil Infrastructure Platform"
date: 2020-10-21 00:00:00
categories: org
draft: false
---

[![]({{ "/images/news/supporter-spotlight-cip-project/cip.png#right" | relative_url }})](https://www.cip-project.org/)

<big>The Reproducible Builds project depends on our [many projects, supporters
and sponsors]({{ "/who/" | relative_url }}). We rely on their financial
support, but they are also valued ambassadors who spread the word about the
Reproducible Builds project and the work that we do.</big>

This is the first installment in a series featuring the projects, companies and
individuals who support the Reproducible Builds project. If you are a supporter
of the Reproducible Builds project (of whatever size) and would like to be
featured here, please let get in touch with us at
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).

However, we are kicking off this series by featuring Urs Gleim and Yoshi
Kobayashi of the [Civil Infrastructure Platform
(CIP)](https://www.cip-project.org/) project.

<br>

**Chris Lamb: Hi Urs and Yoshi, great to meet you. How might you relate the
importance of the Civil Infrastructure Platform to a user who is
non-technical?**

A: The [Civil Infrastructure Platform (CIP)](https://www.cip-project.org/)
project is focused on establishing an open source 'base layer' of
industrial-grade software that acts as building blocks in civil infrastructure
projects. End-users of this critical code include systems for electric power
generation and energy distribution, oil and gas, water and wastewater,
healthcare, communications, transportation, and community management. These
systems deliver essential services, provide shelter, and support social
interactions and economic development. They are society's lifelines, and CIP
aims to contribute to and support these important pillars of modern society.

<br>

**Chris: We have entered an age where our civilisations have become reliant on
technology to keep us alive. Does the CIP believe that the software that
underlies our own safety (and the safety of our loved ones) receives enough
scrutiny today?**

A: For companies developing systems running our infrastructure and keeping our
factories working, it is part of their business to ensure the availability,
uptime, and security of these very systems. However, software complexity
continues to increase, and the efforts spent on those systems is now exploding.
What is missing is a common way of achieving this through refining the same
tools, and cooperating on the hardening and maintenance of standard components
such as the Linux operating system.

<br>

**Chris: How does the Reproducible Builds effort help the Civil Infrastructure
Platform achieve its goals?**

A: Reproducibility helps a great deal in software maintenance. We have a number
of use-cases that should have long-term support of more than 10 years. During
this period, we encounter issues that need to be fixed in the original source
code. But before we make changes to the source code, we need to check whether it
is actually the original source code or not. If we can reproduce exactly the
same binary from the source code even after 10 years, we can start to invest
time and energy into making these fixes.

<br>

**Chris: Can you give us a brief history of the Civil Infrastructure Platform?
Are there any specific 'success stories' that the CIP is particularly proud
of?**

[![]({{ "/images/news/supporter-spotlight-cip-project/debianlts.jpg#right" | relative_url }})](https://wiki.debian.org/LTS)

A: The CIP Project formed in 2016 as a project hosted by Linux Foundation. It
was launched out of necessity to establish an open source framework and
the subsequent software foundation delivers services for civil infrastructure
and economic development on a global scale. Some key milestones we have
achieved as a project include our collaboration with
[Debian](https://debian.org), where we are helping with the [Debian Long Term
Support (LTS)](https://wiki.debian.org/LTS) initiative, which aims to extend
the lifetime of all Debian stable releases to at least 5 years. This is
critical because most control systems for transportation, power plants,
healthcare and telecommunications run on Debian-based embedded systems.

In addition, CIP is focused on [IEC 62443](https://www.isa.org/intech-home/2018/september-october/departments/new-standard-specifies-security-capabilities-for-c),
a standards-based approach to counter security vulnerabilities in industrial
automation and control systems. Our belief is that this work will help mitigate
the risk of cyber attacks, but in order to deal with evolving attacks of this
kind, all of the layers that make up these complex systems (such as system
services and component functions, in addition to the countless operational
layers) must be kept secure. For this reason, the IEC 62443 series is
attracting attention as the *de facto* cyber-security standard.

<br>

**Chris: The Civil Infrastructure Platform project comprises a number of
project members from different industries, with stakeholders across multiple
countries and continents. How does working together with a broad group of
interests help in your effectiveness and efficiency?**

A: Although the members have different products, they share the requirements
and issues when developing sustainable products. In the end, we are driven by
common goals. For the project members, working internationally is simply daily
business. We see this as an advantage over regional or efforts that focus on
narrower domains or markets.

<br>

**Chris: The Civil Infrastructure Platform supports a number of other existing
projects and initiatives in the open source world too. How much do you feel
being a part of the broader free software community helps you achieve your
aims?**

A: Collaboration with other projects is an essential part of how CIP operates —
we want to enable commonly-used software components. It would not make sense to
re-invent solutions that are already established and widely used in product
development. To this end, we have an 'upstream first' policy which means that,
if existing projects need to be modified to our needs or are already working on
issues that we also need, we work directly with them.

<br>

**Chris: Open source software in desktop or user-facing contexts receives a
significant amount of publicity in the media. However, how do you see the
future of free software from an industrial-oriented context?**

A: Open source software has already become an essential part of the industry
and civil infrastructure, and the importance of open source software there is
still increasing. Without open source software, we cannot achieve, run and
maintain future complex systems, such as smart cities and other key pieces of
civil infrastructure.

<br>

[![]({{ "/images/news/supporter-spotlight-cip-project/cip.png#right" | relative_url }})](https://www.cip-project.org/)

**Chris: If someone wanted to know more about the Civil Infrastructure Platform
(or even to get involved) where should they go to look?**

A: We have many avenues to participate and learn more! We have a
[website](https://cip-project.org), a [wiki](https://wiki.linuxfoundation.org/civilinfrastructureplatform/start)
and you can even [follow us on Twitter](https://twitter.com/cip_project).

<br>

---

*For more about the Reproducible Builds project, please see our website at
[reproducible-builds.org]({{ "/" | relative_url }}). If you are interested in
ensuring the ongoing security of the software that underpins our civilisation
and wish to sponsor the Reproducible Builds project, please reach out to the
project by emailing
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).*
