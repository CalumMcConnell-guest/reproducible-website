---
layout: post
title: "Restarting Reproducible Builds IRC meetings"
date: 2020-10-12 08:32:00
draft: false
---

[![]({{ "/images/news/reproducible-irc-meeting/irc.png#right" | relative_url }})]({{ "https://time.is/compare/1800_12_Oct_2020_in_UTC" | relative_url }})

The Reproducible Builds project intends to resume meeting regularly on IRC, starting **today**, Monday October 12th, at [**18:00 UTC**](https://time.is/compare/1800_12_Oct_2020_in_UTC).

Sadly, due to the unprecedented events in 2020, there will be [no in-person Reproducible Builds event this year](https://lists.reproducible-builds.org/pipermail/rb-general/2020-September/002045.html), but please join us on the `#reproducible-builds` channel on [irc.oftc.net](https://oftc.net/). An [editable agenda is available](https://pad.sfconservancy.org/p/reproducible-builds-meeting-agenda). The cadence of these meetings will probably be every two weeks, although this will be discussed and decided on at the first meeting.
